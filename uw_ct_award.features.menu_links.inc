<?php
/**
 * @file
 * uw_ct_award.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_award_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_award-search-result-header:admin/config/system/award_search_result_header.
  $menu_links['menu-site-management_award-search-result-header:admin/config/system/award_search_result_header'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/award_search_result_header',
    'router_path' => 'admin/config/system/award_search_result_header',
    'link_title' => 'Award search result header',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_award-search-result-header:admin/config/system/award_search_result_header',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_award-affiliation:admin/structure/taxonomy/undergrad_award_affiliation.
  $menu_links['menu-site-manager-vocabularies_award-affiliation:admin/structure/taxonomy/undergrad_award_affiliation'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_affiliation',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Award affiliation',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_award-affiliation:admin/structure/taxonomy/undergrad_award_affiliation',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_award-application-details:admin/structure/taxonomy/undergrad_award_detail.
  $menu_links['menu-site-manager-vocabularies_award-application-details:admin/structure/taxonomy/undergrad_award_detail'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_detail',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Award application details',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_award-application-details:admin/structure/taxonomy/undergrad_award_detail',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_award-citizenship:admin/structure/taxonomy/undergrad_award_citizenship.
  $menu_links['menu-site-manager-vocabularies_award-citizenship:admin/structure/taxonomy/undergrad_award_citizenship'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_citizenship',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Award citizenship',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_award-citizenship:admin/structure/taxonomy/undergrad_award_citizenship',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_award-deadline:admin/structure/taxonomy/undergrad_award_deadline.
  $menu_links['menu-site-manager-vocabularies_award-deadline:admin/structure/taxonomy/undergrad_award_deadline'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_deadline',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Award deadline',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_award-deadline:admin/structure/taxonomy/undergrad_award_deadline',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_award-level:admin/structure/taxonomy/undergrad_award_enrollment.
  $menu_links['menu-site-manager-vocabularies_award-level:admin/structure/taxonomy/undergrad_award_enrollment'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_enrollment',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Award level',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_award-level:admin/structure/taxonomy/undergrad_award_enrollment',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_award-process-type:admin/structure/taxonomy/undergrad_award_process.
  $menu_links['menu-site-manager-vocabularies_award-process-type:admin/structure/taxonomy/undergrad_award_process'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_process',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Award process type',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_award-process-type:admin/structure/taxonomy/undergrad_award_process',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_award-program---graduate:admin/structure/taxonomy/uw_departments.
  $menu_links['menu-site-manager-vocabularies_award-program---graduate:admin/structure/taxonomy/uw_departments'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_departments',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Award program - Graduate',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_award-program---graduate:admin/structure/taxonomy/uw_departments',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_award-program---undergraduate:admin/structure/taxonomy/undergrad_award_program.
  $menu_links['menu-site-manager-vocabularies_award-program---undergraduate:admin/structure/taxonomy/undergrad_award_program'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_program',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Award program - Undergraduate',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_award-program---undergraduate:admin/structure/taxonomy/undergrad_award_program',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_award-term:admin/structure/taxonomy/undergrad_award_term.
  $menu_links['menu-site-manager-vocabularies_award-term:admin/structure/taxonomy/undergrad_award_term'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_term',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Award term',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_award-term:admin/structure/taxonomy/undergrad_award_term',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_award-type:admin/structure/taxonomy/undergrad_award_type.
  $menu_links['menu-site-manager-vocabularies_award-type:admin/structure/taxonomy/undergrad_award_type'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_type',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Award type',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_award-type:admin/structure/taxonomy/undergrad_award_type',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Award affiliation');
  t('Award application details');
  t('Award citizenship');
  t('Award deadline');
  t('Award level');
  t('Award process type');
  t('Award program - Graduate');
  t('Award program - Undergraduate');
  t('Award search result header');
  t('Award term');
  t('Award type');

  return $menu_links;
}
